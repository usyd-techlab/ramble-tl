#!/usr/bin/env python2

import os
import sys
import json
import requests
import pyttsx
engine = pyttsx.init()
engine.setProperty('rate', 130)
engine.setProperty('voice', 2)


def usage():
    print 'Takes an input string and translates it back and forth through'
    print '16 languages using Microsoft\'s Translator API, usually resulting'
    print 'in nonsensical but hopefully amusing text as the output.\n'
    print 'It also speaks the Input string, and the Final output string,'
    print 'if you have a text to speech engine installed'
    print 'USAGE:'
    print 'ramble This is a sample string'

def get_access_token(client_id, client_secret):
    uri = 'https://datamarket.accesscontrol.windows.net/v2/OAuth2-13'
    args = {
        'client_id': client_id,
        'client_secret': client_secret,
        'scope': 'http://api.microsofttranslator.com',
        'grant_type': 'client_credentials'
    }
    
    response = requests.post(uri, data=args).json()

    return response['access_token']

def call(path, params):
    base_url = 'http://api.microsofttranslator.com/V2/Ajax.svc'
    resp = requests.get(
        '/'.join([base_url, path]),
        params = params,
        headers = {'Authorization': 'Bearer %s' % access_token}
    )

    resp.encoding = 'UTF-8-sig'

    return resp.json()

def translate(text_to_translate, to_lang, from_lang):
    params = {
        'text': text_to_translate.encode('utf8'),
        'to': to_lang,
        'from': from_lang,
        'contentType': 'text/plain',
        'category': 'general'
    }

    return call('Translate', params)

def get_languages():
    return call('GetLanguagesForTranslate', '')

def main(text_to_translate):
    if text_to_translate == 'getlang':
        langs = get_languages()
        for lang in langs:
                print lang
    else:
        langs = ['en', 'tlh', 'en', 'bg', 'en', 'da', 'en', 'fi', 'en', 'de', \
            'en', 'ja', 'en', 'zh-CHS', 'en', 'ko', 'en', 'ms', 'en', 'no',  \
            'en', 'pt', 'en', 'fa', 'en', 'mww', 'en', 'th', 'en', 'tr', 'en',\
            'cy', 'en', 'he', 'en', 'ht', 'en', 'fr', 'en' ]
        i = 0
        print u'input  : ' + text_to_translate
        engine.say(text_to_translate)
        engine.runAndWait()
        while i < len(langs) - 1:
            to_lang = langs[i + 1]
            from_lang = langs[i]
            text_to_translate = translate(text_to_translate, to_lang, from_lang)
            if to_lang == 'en':
                print from_lang + u' to ' + 'en: ' + text_to_translate
            i += 1
            
        return text_to_translate

if __name__ == '__main__':
    if len(sys.argv) == 1:
        usage()
        sys.exit(1)



    else:
        try:
            rambledir = os.path.dirname(os.path.realpath(__file__))
            with open(os.path.join(rambledir, 'ramble.json'), 'rb') as credentials_file:
                credentials = json.load(credentials_file)
            client_id = credentials['client_id']
            client_secret = credentials['client_secret']
      
        except IOError:
            print 'You need a client id and secret to use the Microsoft Translator API.'
            print 'Information is available here: http://www.microsoft.com/en-us/translator/getstarted.aspx\n'
            print 'Once you have your keys, create a json file called \'ramble.json\' in the same directory as'
            print 'the ramble script, containing the following:\n'
            print '{'
            print '	"client_id": "your_client_id_here",'
            print '	"client_secret": "your_client_secret_here"'
            print '}'
            sys.exit(1)

        text_to_translate = ' '.join(sys.argv[1:])
        try:
            access_token = get_access_token(client_id, client_secret)
            text_to_translate = main(text_to_translate)
        except KeyboardInterrupt:
            print 'Cancelled. Stopping.'
        #except:
         #  print 'Something went wrong.'
         # sys.exit(1)



engine.say(text_to_translate)
engine.runAndWait()
